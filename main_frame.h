///-----------------------------------------------------------------
///
/// @file      main_frame.h
/// @author    joechristopherpaulamanse
/// Created:   12/11/2014 12:38:53 AM
/// @section   DESCRIPTION
///            MainFrame class declaration
///
///------------------------------------------------------------------

#ifndef __MAIN_FRAME_H__
#define __MAIN_FRAME_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/statbmp.h>
#include <wx/checkbox.h>
#include <wx/clrpicker.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/scrolwin.h>
#include <wx/button.h>
#include <wx/panel.h>
////Header Include End

#include "drawing_panel.h"

////Dialog Style Start
#undef MainFrame_STYLE
#define MainFrame_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class MainFrame : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();
		
		void openImageButtonClick(wxCommandEvent& event);
		void drawLineButtonClick(wxCommandEvent& event);
		void drawRectangleButtonClick(wxCommandEvent& event);
		void drawEllipseButtonClick(wxCommandEvent& event);
		void saveImageButtonClick(wxCommandEvent& event);
        void OnSize(wxSizeEvent& event);
        
        void MainFrameSize(wxSizeEvent& event);
		void mainColorColourChanged(wxColourPickerEvent& event);
		void secondaryColorColourChanged(wxColourPickerEvent& event);
		void fillCheckBoxClick(wxCommandEvent& event);
		void WxButton1Click(wxCommandEvent& event);
		void newImageButtonClick(wxCommandEvent& event);
        
	public:
		MainFrame(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Image Factory"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = MainFrame_STYLE);
		virtual ~MainFrame();
		void CreateNewImage(wxSize size);
		
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxButton *newImageButton;
		wxStaticBitmap *WxStaticBitmap1;
		wxCheckBox *fillCheckBox;
		wxColourPickerCtrl *secondaryColor;
		wxColourPickerCtrl *mainColor;
		wxTextCtrl *currentToolLabel;
		wxButton *saveImageButton;
		wxBitmapButton *drawEllipseButton;
		wxBitmapButton *drawRectangleButton;
		wxBitmapButton *drawLineButton;
		wxScrolledWindow *scrollWindow;
		wxButton *openImageButton;
		wxPanel *mainPanel;
		////GUI Control Declaration End
		
		DrawingPanel *aDrawingPanel;
		wxBitmap bitmapImage;
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_NEWIMAGEBUTTON = 1019,
			ID_WXSTATICBITMAP1 = 1018,
			ID_FILLCHECKBOX = 1017,
			ID_SECONDARYCOLOR = 1016,
			ID_MAINCOLOR = 1015,
			ID_CURRENTTOOLLABEL = 1014,
			ID_SAVEIMAGEBUTTON = 1012,
			ID_DRAWELLIPSEBUTTON = 1010,
			ID_DRAWRECTANGLEBUTTON = 1008,
			ID_DRAWLINEBUTTON = 1007,
			ID_SCROLLWINDOW = 1005,
			ID_OPENIMAGEBUTTON = 1004,
			ID_MAINPANEL = 1003,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		
	private:
        bool drawingPanelExists;
        
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
		
		void showNewImageDialog();
		void showOpenImageDialog();
		
		void CreateDrawingPanel(wxBitmap bitmap);
		void EnableControls(bool enable);
};

#endif
