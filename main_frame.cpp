///-----------------------------------------------------------------
///
/// @file      MainFrame.cpp
/// @author    joechristopherpaulamanse
/// Created:   12/11/2014 12:38:53 AM
/// @section   DESCRIPTION
///            MainFrame class implementation
///
///------------------------------------------------------------------

#include "main_frame.h"
#include "new_image_dialog.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
#include "Images/MainFrame_WxStaticBitmap1_XPM.xpm"
#include "Images/mainframe_drawellipsebutton.xpm"
#include "Images/mainframe_drawrectanglebutton.xpm"
#include "Images/mainframe_drawlinebutton.xpm"
////Header Include End

//----------------------------------------------------------------------------
// MainFrame
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(MainFrame,wxFrame)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(MainFrame::OnClose)
	EVT_SIZE(MainFrame::MainFrameSize)
	EVT_BUTTON(ID_NEWIMAGEBUTTON,MainFrame::newImageButtonClick)
	EVT_CHECKBOX(ID_FILLCHECKBOX,MainFrame::fillCheckBoxClick)
	EVT_COLOURPICKER_CHANGED(ID_SECONDARYCOLOR,MainFrame::secondaryColorColourChanged)
	EVT_COLOURPICKER_CHANGED(ID_MAINCOLOR,MainFrame::mainColorColourChanged)
	EVT_BUTTON(ID_SAVEIMAGEBUTTON,MainFrame::saveImageButtonClick)
	EVT_BUTTON(ID_DRAWELLIPSEBUTTON,MainFrame::drawEllipseButtonClick)
	EVT_BUTTON(ID_DRAWRECTANGLEBUTTON,MainFrame::drawRectangleButtonClick)
	EVT_BUTTON(ID_DRAWLINEBUTTON,MainFrame::drawLineButtonClick)
	EVT_BUTTON(ID_OPENIMAGEBUTTON,MainFrame::openImageButtonClick)
END_EVENT_TABLE()
////Event Table End

MainFrame::MainFrame(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	CreateGUIControls();
	
	// Initialize all image handlers
	wxInitAllImageHandlers();
}

MainFrame::~MainFrame()
{
}

void MainFrame::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	wxInitAllImageHandlers();   //Initialize graphic format handlers

	mainPanel = new wxPanel(this, ID_MAINPANEL, wxPoint(0, 0), wxSize(781, 450));
	mainPanel->SetBackgroundColour(wxColour(0,128,255));

	openImageButton = new wxButton(mainPanel, ID_OPENIMAGEBUTTON, _("Open"), wxPoint(3, 89), wxSize(85, 29), 0, wxDefaultValidator, _("openImageButton"));

	scrollWindow = new wxScrolledWindow(mainPanel, ID_SCROLLWINDOW, wxPoint(89, -32), wxSize(693, 449), wxVSCROLL | wxHSCROLL | wxALWAYS_SHOW_SB);
	scrollWindow->SetBackgroundColour(wxColour(192,192,192));

	wxBitmap drawLineButton_BITMAP (MainFrame_drawLineButton_XPM);
	drawLineButton = new wxBitmapButton(mainPanel, ID_DRAWLINEBUTTON, drawLineButton_BITMAP, wxPoint(7, 182), wxSize(31, 29), wxNO_BORDER | wxBU_AUTODRAW, wxDefaultValidator, _("drawLineButton"));
	drawLineButton->SetBackgroundColour(wxColour(0,128,255));

	wxBitmap drawRectangleButton_BITMAP (MainFrame_drawRectangleButton_XPM);
	drawRectangleButton = new wxBitmapButton(mainPanel, ID_DRAWRECTANGLEBUTTON, drawRectangleButton_BITMAP, wxPoint(46, 182), wxSize(31, 33), wxNO_BORDER | wxBU_AUTODRAW, wxDefaultValidator, _("drawRectangleButton"));
	drawRectangleButton->SetBackgroundColour(wxColour(0,128,255));

	wxBitmap drawEllipseButton_BITMAP (MainFrame_drawEllipseButton_XPM);
	drawEllipseButton = new wxBitmapButton(mainPanel, ID_DRAWELLIPSEBUTTON, drawEllipseButton_BITMAP, wxPoint(7, 215), wxSize(30, 33), wxNO_BORDER | wxBU_AUTODRAW, wxDefaultValidator, _("drawEllipseButton"));
	drawEllipseButton->SetBackgroundColour(wxColour(0,128,255));

	saveImageButton = new wxButton(mainPanel, ID_SAVEIMAGEBUTTON, _("Save"), wxPoint(3, 120), wxSize(85, 29), 0, wxDefaultValidator, _("saveImageButton"));

	currentToolLabel = new wxTextCtrl(mainPanel, ID_CURRENTTOOLLABEL, _("Select tool"), wxPoint(6, 152), wxSize(77, 22), wxTE_CENTRE, wxDefaultValidator, _("currentToolLabel"));
	currentToolLabel->Enable(false);

	mainColor = new wxColourPickerCtrl(mainPanel, ID_MAINCOLOR, *wxBLACK, wxPoint(3, 291), wxSize(82, 27), wxCLRP_DEFAULT_STYLE, wxDefaultValidator, _("mainColor"));
	mainColor->SetFont(wxFont(9, wxSWISS, wxNORMAL, wxNORMAL, false, _("MS Shell Dlg")));

	secondaryColor = new wxColourPickerCtrl(mainPanel, ID_SECONDARYCOLOR, wxColour(255,255,255), wxPoint(3, 321), wxSize(82, 27), wxCLRP_DEFAULT_STYLE, wxDefaultValidator, _("secondaryColor"));
	secondaryColor->SetFont(wxFont(9, wxSWISS, wxNORMAL, wxNORMAL, false, _("MS Shell Dlg")));

	fillCheckBox = new wxCheckBox(mainPanel, ID_FILLCHECKBOX, _("Fill"), wxPoint(7, 352), wxSize(77, 20), 0, wxDefaultValidator, _("fillCheckBox"));

	wxBitmap WxStaticBitmap1_BITMAP(MainFrame_WxStaticBitmap1_XPM);
	WxStaticBitmap1 = new wxStaticBitmap(mainPanel, ID_WXSTATICBITMAP1, WxStaticBitmap1_BITMAP, wxPoint(5, 6), wxSize(76, 42),wxRAISED_BORDER);
	WxStaticBitmap1->SetBackgroundColour(wxColour(255,255,255));

	newImageButton = new wxButton(mainPanel, ID_NEWIMAGEBUTTON, _("New"), wxPoint(3, 57), wxSize(85, 28), 0, wxDefaultValidator, _("newImageButton"));

	SetTitle(_("Image Factory"));
	SetIcon(wxNullIcon);
	SetSize(8,8,797,488);
	Center();
	
	////GUI Items Creation End
	
	scrollWindow->SetScrollbars(10,10,1000/10,1000/10);
	currentToolLabel->SetValue(_("Line"));
	
	drawingPanelExists = false;
	
    // Disable controls at first
    this->EnableControls(false);
}

void MainFrame::OnClose(wxCloseEvent& event)
{
    // Check if image is saved
	Destroy();
}

void MainFrame::EnableControls(bool enable) {
    drawLineButton->Enable(enable);
    drawRectangleButton->Enable(enable);
    drawEllipseButton->Enable(enable);
    saveImageButton->Enable(enable);
    fillCheckBox->Enable(enable);
    mainColor->Enable(enable);
    secondaryColor->Enable(enable);
}

void MainFrame::CreateDrawingPanel(wxBitmap bitmap) {
    // If an image is already open, discard it and open new
    if (drawingPanelExists) {
        delete aDrawingPanel;
    }
    
    this->bitmapImage = bitmap;
    aDrawingPanel = new DrawingPanel(scrollWindow, bitmapImage, wxPoint(0,0), wxSize(bitmapImage.GetWidth(),bitmapImage.GetHeight()));
    
    // To avoid flickering
    aDrawingPanel->SetDoubleBuffered(true);
    aDrawingPanel->SetBackgroundStyle(wxBG_STYLE_CUSTOM);
    
    // Set tool values
    aDrawingPanel->SetImageCanvasTool(ImageCanvasToolLine);
    currentToolLabel->SetValue(_("Line"));
    aDrawingPanel->SetToolColor(mainColor->GetColour());
    aDrawingPanel->SetSecondaryToolColor(secondaryColor->GetColour());
    aDrawingPanel->SetFill(fillCheckBox->IsChecked());
    
    scrollWindow->SetScrollbars(5,5, bitmapImage.GetWidth()/5, bitmapImage.GetHeight()/5);
    
    drawingPanelExists = true;
    
    // Enable controls
    this->EnableControls(true);
}

/*
 * openImageButtonClick
 */
void MainFrame::openImageButtonClick(wxCommandEvent& event)
{
    if (drawingPanelExists && aDrawingPanel->IsEdited()) {
        int response = wxMessageBox(_("Opening a new image will discard current changes. Continue?"),_("Warning"), wxYES | wxNO);
        if (response == wxYES) {
            this->showOpenImageDialog();
        }
    } else {
        this->showOpenImageDialog();
    }
}

void MainFrame::showOpenImageDialog() {
    wxFileDialog *openImageFileDialog =  new wxFileDialog(this, _("Open Image"), _(""), _(""), _("Image Files (*.bmp; *.gif; *.jpeg; *.jpg; *.png; *.tif)|*.bmp;*.gif;*.jpeg;*.jpg;*.png;*.tif|BMP files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif|JPEG files (*.jpeg; *.jpg)|*.jpeg;*.jpg|PNG files (*.png)|*.png|TIFF files (*.tif)|*.tif"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (openImageFileDialog->ShowModal() == wxID_OK) {
        wxBitmap aBitmap = wxBitmap(openImageFileDialog->GetPath(), wxBITMAP_TYPE_ANY);
        this->CreateDrawingPanel(aBitmap);
    }
    
    openImageFileDialog->Destroy();
}

void MainFrame::saveImageButtonClick(wxCommandEvent& event)
{
	wxFileDialog *saveFileDialog = new wxFileDialog(this, _("Save Image"), _(""), _(""), _("BMP file (*.bmp)|*.bmp|GIF file (*.gif)|*.gif|JPEG file (*.jpg)|*.jpg|PNG file (*.png)|*.png|TIFF file (*.tif)|*.tif"), wxFD_SAVE);
    
	if (saveFileDialog->ShowModal() == wxID_OK)
	{
		wxString savePath = saveFileDialog->GetPath();
		wxBitmap* currentBitmap = aDrawingPanel->GetBackgroundBitmap();
		wxImage currentImage = currentBitmap->ConvertToImage();
		currentImage.SaveFile(savePath); // File type depends on extension
		
		// Set edited to false since image is already saved
		aDrawingPanel->SetEdited(false);
	} else {
        saveFileDialog->Close();
    }
    
    saveFileDialog->Destroy();
}

/*
 * drawLineButtonClick
 */
void MainFrame::drawLineButtonClick(wxCommandEvent& event)
{
    aDrawingPanel->SetImageCanvasTool(ImageCanvasToolLine);
    currentToolLabel->SetValue(_("Line"));
}

/*
 * drawRectangleButtonClick
 */
void MainFrame::drawRectangleButtonClick(wxCommandEvent& event)
{
    aDrawingPanel->SetImageCanvasTool(ImageCanvasToolRectangle);
    currentToolLabel->SetValue(_("Rectangle"));
}

/*
 * drawEllipseButtonClick
 */
void MainFrame::drawEllipseButtonClick(wxCommandEvent& event)
{
    aDrawingPanel->SetImageCanvasTool(ImageCanvasToolEllipse);
    currentToolLabel->SetValue(_("Ellipse"));
}

/*
 * MainFrameSize
 */
void MainFrame::MainFrameSize(wxSizeEvent& event)
{
    wxSize size = event.GetSize();
    mainPanel->SetSize(size);
    // Offset the size by the scrollWindow's origin and scrollbar sizes
    wxPoint originScrollWindow = scrollWindow->GetPosition();
    wxSize newSize = wxSize(size.GetWidth() - originScrollWindow.x - 16, size.GetHeight() - originScrollWindow.y - 37);
    scrollWindow->SetSize(newSize);
}

/*
 * mainColorColourChanged
 */
void MainFrame::mainColorColourChanged(wxColourPickerEvent& event)
{
    aDrawingPanel->SetToolColor(mainColor->GetColour());
}

/*
 * secondaryColorColourChanged
 */
void MainFrame::secondaryColorColourChanged(wxColourPickerEvent& event)
{
    aDrawingPanel->SetSecondaryToolColor(secondaryColor->GetColour());
}

/*
 * fillCheckBoxClick
 */
void MainFrame::fillCheckBoxClick(wxCommandEvent& event)
{
    aDrawingPanel->SetFill(fillCheckBox->IsChecked());
}

/*
 * newImageButtonClick
 */
void MainFrame::newImageButtonClick(wxCommandEvent& event)
{
    if (drawingPanelExists && aDrawingPanel->IsEdited()) {
        int response = wxMessageBox(_("Opening a new image will discard current changes. Continue?"),_("Warning"), wxYES | wxNO);
        if (response == wxYES) {
            this->showNewImageDialog();
        }
    } else {
        this->showNewImageDialog();
    }
    
}

void MainFrame::showNewImageDialog() {
    NewImageDialog* dialog = new NewImageDialog(this);
    
	if (dialog->ShowModal() == wxID_OK) {
        this->CreateNewImage(dialog->GetNewImageSize());
    }
    
    dialog->Destroy();
}

void MainFrame::CreateNewImage(wxSize size) {
    wxImage anImage = wxImage(size);
    anImage.Clear(255);
    wxBitmap aBitmap = wxBitmap(anImage);
    this->CreateDrawingPanel(aBitmap);
}
