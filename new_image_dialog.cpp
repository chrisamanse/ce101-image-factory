///-----------------------------------------------------------------
///
/// @file      new_image_dialog.cpp
/// @author    joechristopherpaulamanse
/// Created:   2/9/2015 1:54:28 PM
/// @section   DESCRIPTION
///            NewImageDialog class implementation
///
///------------------------------------------------------------------

#include "new_image_dialog.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// NewImageDialog
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(NewImageDialog,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(NewImageDialog::OnClose)
	EVT_BUTTON(ID_CANCELBUTTON,NewImageDialog::cancelButtonClick)
	EVT_BUTTON(ID_OKBUTTON,NewImageDialog::okButtonClick)
END_EVENT_TABLE()
////Event Table End

NewImageDialog::NewImageDialog(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

NewImageDialog::~NewImageDialog()
{
} 

void NewImageDialog::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(0, 0), wxSize(402, 197));

	okButton = new wxButton(WxPanel1, ID_OKBUTTON, _("OK"), wxPoint(312, 159), wxSize(75, 25), 0, wxDefaultValidator, _("okButton"));

	cancelButton = new wxButton(WxPanel1, ID_CANCELBUTTON, _("Cancel"), wxPoint(227, 159), wxSize(75, 25), 0, wxDefaultValidator, _("cancelButton"));

	widthLabel = new wxStaticText(WxPanel1, ID_WIDTHLABEL, _("Width:"), wxPoint(47, 40), wxDefaultSize, 0, _("widthLabel"));

	heightLabel = new wxStaticText(WxPanel1, ID_HEIGHTLABEL, _("Height:"), wxPoint(45, 80), wxDefaultSize, 0, _("heightLabel"));

	widthText = new wxTextCtrl(WxPanel1, ID_WIDTHTEXT, _("1280"), wxPoint(82, 40), wxSize(121, 19), 0, wxDefaultValidator, _("widthText"));

	heightText = new wxTextCtrl(WxPanel1, ID_HEIGHTTEXT, _("720"), wxPoint(82, 80), wxSize(121, 19), 0, wxDefaultValidator, _("heightText"));

	SetTitle(_("NewImageDialog"));
	SetIcon(wxNullIcon);
	SetSize(8,8,411,224);
	Center();
	
	////GUI Items Creation End
}

void NewImageDialog::OnClose(wxCloseEvent& /*event*/)
{
	EndModal(wxID_CANCEL);
}

/*
 * okButtonClick
 */
void NewImageDialog::okButtonClick(wxCommandEvent& event)
{
    long widthLong = 0;
    long heightLong = 0;
    wxString widthString = widthText->GetLineText(0);
    wxString heightString = heightText->GetLineText(0);
    
    if (!widthString.ToLong(&widthLong) || !heightString.ToLong(&heightLong)) {
        // Not integer
        wxMessageBox("Please input valid dimensions.", "Error", wxOK, this);
    } else {
        this->newImageSize = wxSize((int)widthLong,(int)heightLong);
        EndModal(wxID_OK);
    }
}

/*
 * cancelButtonClick
 */
void NewImageDialog::cancelButtonClick(wxCommandEvent& event)
{
    EndModal(wxID_CANCEL);
}

wxSize NewImageDialog::GetNewImageSize() {
    return this->newImageSize;
}
