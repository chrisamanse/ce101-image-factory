///-----------------------------------------------------------------
///
/// @file      new_image_dialog.h
/// @author    joechristopherpaulamanse
/// Created:   2/9/2015 1:54:28 PM
/// @section   DESCRIPTION
///            NewImageDialog class declaration
///
///------------------------------------------------------------------

#ifndef __NEWIMAGEDIALOG_H__
#define __NEWIMAGEDIALOG_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/panel.h>
////Header Include End

////Dialog Style Start
#undef NewImageDialog_STYLE
#define NewImageDialog_STYLE wxCAPTION | wxSYSTEM_MENU | wxSTAY_ON_TOP | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class NewImageDialog : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		NewImageDialog(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("NewImageDialog"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = NewImageDialog_STYLE);
		virtual ~NewImageDialog();
		wxSize GetNewImageSize();
		void okButtonClick(wxCommandEvent& event);
		void cancelButtonClick(wxCommandEvent& event);
		
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxTextCtrl *heightText;
		wxTextCtrl *widthText;
		wxStaticText *heightLabel;
		wxStaticText *widthLabel;
		wxButton *cancelButton;
		wxButton *okButton;
		wxPanel *WxPanel1;
		////GUI Control Declaration End
		
		wxSize newImageSize;
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_HEIGHTTEXT = 1007,
			ID_WIDTHTEXT = 1006,
			ID_HEIGHTLABEL = 1005,
			ID_WIDTHLABEL = 1004,
			ID_CANCELBUTTON = 1003,
			ID_OKBUTTON = 1002,
			ID_WXPANEL1 = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
