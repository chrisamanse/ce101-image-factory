//---------------------------------------------------------------------------
//
// Name:        image_factory_app.h
// Author:      joechristopherpaulamanse
// Created:     12/11/2014 12:38:52 AM
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __MAINFRAMEApp_h__
#define __MAINFRAMEApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class MainFrameApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
