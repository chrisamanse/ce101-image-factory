//---------------------------------------------------------------------------
//
// Name:        image_factory_app.cpp
// Author:      joechristopherpaulamanse
// Created:     12/11/2014 12:38:52 AM
// Description: 
//
//---------------------------------------------------------------------------

#include "image_factory_app.h"
#include "main_frame.h"

IMPLEMENT_APP(MainFrameApp)

bool MainFrameApp::OnInit()
{
    // Show main frame on start
    MainFrame* frame = new MainFrame(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}
 
int MainFrameApp::OnExit()
{
	return 0;
}
